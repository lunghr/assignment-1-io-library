%define NEW_LINE 0xA
%define TAB_SYMBOL 0x9
%define SPACE_SYMBOL 0x20
%define ZERO 0x0


section .text

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    xor rdi, rdi ;rdi используется для передачи кода ошибки, если такая возникает
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    push rcx
    xor rcx, rcx
    .loop:
    cmp byte [rdi+rcx], 0
    je .end
    inc rcx
    jmp .loop

    .end:
    mov rax, rcx
    pop rcx
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov rdx, rax
    mov rsi, rdi
    mov rax, 1
    mov rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rsi, rsp
    mov rdi, 1
    mov rdx, 1
    syscall
    pop rdi
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    push rdi
    mov rdi, NEW_LINE
    call print_char
    pop rdi
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push r12
    
    mov rax, rdi
    mov rcx, 10
    mov r12, rsp

    .loop:
    xor     rdx, rdx;
    div     rcx
    add     rdx, '0'
    dec     rsp
    mov     byte[rsp], dl
    test    rax, rax
    jnz     .loop

    .print:
    mov dil, byte[rsp]
    call print_char
    inc rsp
    cmp rsp, r12
    jnz .print

    pop r12


    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    mov rax, rdi
    test rax, rax
    jns .print_uint
    neg rax
    push rax
    mov rdi, '-'
    call print_char
    pop rax
    .print_uint:
    mov rdi, rax
    call print_uint

    ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:

    xor rax, rax

    .compare:
    mov al, byte[rdi]
    cmp al, byte[rsi]
    jne .not_equal

    test al, al
    je .equal

    inc rdi
    inc rsi
    jmp .compare

    .not_equal:
    xor rax, rax
    ret

    .equal:
    mov rax, 1
    ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    dec rsp
    mov rsi, rsp
    mov rdx, 1
    syscall

    test rax, rax 
    jle .end_of_stream

    movzx rax, byte[rsp]
    inc rsp
    ret
    
    .end_of_stream:
    xor rax, rax
    inc rsp 
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push r12
    push r13
    push r14
    push r15

    mov r12, rdi
    mov r13, rsi
    xor r14, r14
    test rsi, rsi ;if buffer 0 -> rax 0
    jz .err

    .loop:
    call read_char
    cmp rax, SPACE_SYMBOL
    je .check
    cmp rax, NEW_LINE
    je .check
    cmp rax, TAB_SYMBOL
    je .check
    cmp rax, ZERO
    je .complete
    mov r14, 1
    jmp .write

    .write:
    cmp r15, r13
    jg .err
    mov [r12+r15], rax
    inc r15
    jmp .loop

    .err:
    mov rax, 0
    jmp .end

    .check:
    test r14, r14
    jz .loop
    jmp .complete
    
    .complete:
    mov rax, r12
    mov rdx, r15
    jmp .end

    .end:
    pop r15
    pop r14
    pop r13
    pop r12
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    mov rcx, 10
    push r12
    push r13
   

    .read:
    mov r13b, byte[rdi+r12]
    cmp r13b, '0'
    jl .end
    cmp r13b, '9'
    jg .end

    sub r13b, '0'
    mul rcx
    add al, r13b
    inc r12
    jmp .read

    .end:
    mov rdx, r12
    pop r12
    pop r13
    ret


; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
   push r14
   push r15  

   xor r14, r14

   .copy:
   mov r15b, byte[rdi+r14]
   test r15, r15
   jz .end
   mov byte[rsi+r14], r15b
   inc r14
   cmp r14, rdx
   jg .err
   jmp .copy

   .err:
   xor r14, r14
   jmp .end

   .end:
   mov byte[rsi+r14], 0
   mov rax, r14
   pop r15
   pop r14
   ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    push rdi
    cmp byte[rdi], '-'
    je .negative
    call parse_uint
    jmp .end

    .negative:
    inc rdi
    call parse_uint
    inc rdx
    neg rax
    jmp .end

    .end:
    pop rdi
    ret
